<?php
/* Add Theme Supports */

/* add posthumbnail support */
add_theme_support( 'post-thumbnails' );
add_image_size( 'hero_image', 1920, 9999 ); //300 pixels wide (and unlimited height)



/* Menu Registrations */
register_nav_menus ( array (
    'primary' => _('Primary Menu'),
    'footer-menu' => _('Footer Menu'),
    'social-media-menu' => _('Social Media'),
    'order-line-link' => _('Order Online Button'),
    'mobile-menu' => _('Mobile Menu')
));



// limit search to only pages
function mstSearchFilter($query) {
    $post_type = 'page';
    $posts_per_page = 50;
    $search_categories = array();
    if($query->is_main_query()){
        if ($query->is_search) {
            $query->set('post_type', $post_type);
            $query->set('posts_per_page', $posts_per_page);
            $query->set('meta_key', 'course-summary');
            //$query->set('meta_value', 'value');
//	  $query->set('post__in', $search_categories);
//     $query->set('post_parent', 'IN (141,145,143)');
//	  $query->set('post_parent', '145');
//	  $query->set('post_parent', '141');
        }
    }
}
add_filter('pre_get_posts','mstSearchFilter');



/* Mobile Detection and Content Variation */

/* Include mobile detection */
include 'includes/Mobile_Detect.php';
$deviceDetect = new Mobile_Detect();

/**
 * Get site url for links
 *
 */
function url_shortcode() {
    return get_bloginfo('url');
}
add_shortcode('url','url_shortcode');
add_post_type_support( 'page', 'excerpt' );


?>
