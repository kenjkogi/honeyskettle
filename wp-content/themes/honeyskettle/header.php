<?php
/**
 * The Header for our theme.
 *
 * @package WordPress
 * @subpackage My_Security_Training
 * @since Twenty Eleven 1.0
 */
global $deviceDetect;
?>
<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<html>
<head>
    <title><?php
        /*
         * Print the <title> tag based on what is being viewed.
         */
        global $page, $paged;

        wp_title( '|', true, 'right' );

        // Add the blog name.
        bloginfo( 'name' );

        // Add the blog description for the home/front page.
        $site_description = get_bloginfo( 'description', 'display' );
        if ( $site_description && ( is_home() || is_front_page() ) )
            echo " | $site_description";


        ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/css/bootstrap.min.css" integrity="sha384-VCmXjywReHh4PwowAiWNagnWcLhlEJLA5buUprzK8rxFgeH0kww/aWY76TfkUoSX" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
  <link rel="stylesheet" href="https://use.typekit.net/srw1cep.css">
    <?php
    /* We add some JavaScript to pages with the comment form
     * to support sites with threaded comments (when in use).
     */
    if ( is_singular() && get_option( 'thread_comments' ) )
        wp_enqueue_script( 'comment-reply' );

    /* Always have wp_head() just before the closing </head>
     * tag of your theme, or you will break many plugins, which
     * generally use this hook to add elements to <head> such
     * as styles, scripts, and meta tags.
     */
    wp_head();
    ?>
</head>
<body>
    <div id="superHeader">
      <div class="container-fluid desktop-menu">
        <div class="row">
          <div class="col col-12 col-lg-2">
            <div class="order-online-header-link">
                <?php
                $hkmainmenu = array (
                    'theme_location' 	=> 'order-line-link',
                    'container' 		=> 'nav',
                    'container_id'		=> 'order-line-link',
                    'menu_class'		=> 'navbar list-unstyled'
                );
                wp_nav_menu($hkmainmenu);
                ?>
            </div>
          </div>
          <div class="col col-12 col-lg-8">
            <div class="header-main-menu">
                <?php
                $hkmainmenu = array (
                    'theme_location' 	=> 'primary',
                    'container' 		=> 'nav',
                    'container_id'		=> 'mainmenu',
                    'menu_class'		=> 'navbar list-unstyled'
                );
                wp_nav_menu($hkmainmenu);
                ?>
            </div>
          </div>
          <div class="col col-12 col-lg-2">
            <div class="header-menu-social-media">
                <?php
                $hksocialmmenu = array (
                    'theme_location' 	=> 'social-media-menu',
                    'container' 		=> 'nav',
                    'container_id'		=> 'header-social-media',
                    'menu_class'		=> 'navbar list-unstyled',
                    'container_class' 	=> 'social-media-menu'
                );
                wp_nav_menu($hksocialmmenu);
                ?>
            </div>
          </div>
        </div>

      </div>
      <div class="mobile-menu">

        <div class="mobile-menu-trigger-wrapper">
          <button id="mobile-menu-trigger">Menu</button>
        </div>


        <div class="mobile-logo-wrapper">
          <a href="/"><img class="mobile-logo" src="/wp-content/themes/honeyskettle/images/logo-honeyskettle.png" alt="Honey's Kettle Logo"></a>
        </div>

        <div class="row mobile-menu-items">
          <div class="col col-12">
            <div class="header-main-menu">
                <?php
                $hkmainmenu = array (
                    'theme_location' 	=> 'mobile-menu',
                    'container' 		=> 'nav',
                    'container_id'		=> 'mainmenu',
                    'menu_class'		=> 'navbar list-unstyled'
                );
                wp_nav_menu($hkmainmenu);
                ?>
            </div>
          </div>
          <div class="col col-12">
            <div class="header-menu-social-media">
                <?php
                $hksocialmmenu = array (
                    'theme_location' 	=> 'social-media-menu',
                    'container' 		=> 'nav',
                    'container_id'		=> 'header-social-media',
                    'menu_class'		=> 'navbar list-unstyled',
                    'container_class' 	=> 'social-media-menu',
                    'menu_id' 	=> 'menu-social-media-mobile'
                );
                wp_nav_menu($hksocialmmenu);
                ?>
            </div>
          </div>
        </div>

      </div>

    </div>

    <div id="superContent">
