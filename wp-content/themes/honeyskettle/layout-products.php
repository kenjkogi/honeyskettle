<?php
/**
 * Template Name: Merch Template
 * The template for displaying the merch page.
 *
 * This is the template that displays Merchendise page.
 *
 * @package honey's Kettle
 * @since honeyskettle 1.0
 */

?>


<?php get_header(); ?>

<div class="container-fluid">
  <ul class="slides list-unstyled row">

      <?php
      $args = array('category' => '5', 'order' => 'ASC');
      $heroposts = get_posts($args);
      foreach ($heroposts as $post) :
          setup_postdata($post);
          $heroimgurl = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
          $curvedimage = get_post_meta($post->ID, 'merch-image', true);
          $sliderContent = get_the_content();
          $product_price = get_post_meta($post->ID, 'merch-price', true);
          $product_title = get_post_meta($post->ID, 'merch-title', true);
          $product_image = get_post_meta($post->ID, 'merch-image', true);
          $product_id = 'product-' . $post->ID;
          ?>

        <li class="col-4 col">
          <div class="home-hero-content" style="background-image: url(<?php echo $heroimgurl; ?>);">
            <div class="col-sm-12" style="background-image: url("<?php   ?>")">
              <img class="img-responsive imgcentered" src="<?php echo $curvedimage; ?>" alt="">
              <span class="bold"><?php the_content(); ?></span>


              <div id="<?php echo $product_id?>"></div>
              <script>
                setTimeout( function () {
                  paypal.Buttons( {
                    style: {
                      layout:  'horizontal',
                      color:   'white',
                      shape:   'rect',
                      label:   'buynow',
                      tagline: false
                    },
                    createOrder: function ( data, actions ) {
                      // This function sets up the details of the transaction, including the amount and line item details.
                      return actions.order.create( {
                        purchase_units: [
                          {
                            amount: {
                              value: '<?php echo $product_price ?>',
                              total: '<?php echo $product_price ?>',
                              description: '<?php echo $product_title ?>'

                            },
                            description: '<?php echo $product_title ?>',
                            //items: [
                            //  {
                            //    name: "<?php //echo $product_title ?>//",
                            //    description: "<?php //echo $product_title ?>//",
                            //    unit_amount: "<?php //echo $product_price ?>//",
                            //    quantity: 1
                            //
                            //  }
                            //],
                            payment_descriptor: "Honey's Kettle"
                          }
                        ]
                      } );
                    },
                    onApprove: function ( data, actions ) {
                      // This function captures the funds from the transaction.
                      return actions.order.capture().then( function ( details ) {
                        // This function shows a transaction success message to your buyer.
                        alert( 'Transaction completed by ' + details.payer.name.given_name );
                      } );
                    }
                  } ).render( '#<?php echo $product_id ?>' );
                  //This function displays Smart Payment Buttons on your web page.
                }, 1000 );

              </script>
            </div>
          </div>
        </li>


      <?php
      endforeach;
      wp_reset_postdata();
      ?>
  </ul>
</div>

<?php get_footer(); ?>
