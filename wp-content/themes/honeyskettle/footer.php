</div> <?php /* end superContent */ ?>


<div id="superFooter">

  <div class="container">
    <div class="footer-mail-signup">
      Stay in the loop:
      <input type="text" placeholder="john.doe@email.com">
      <button class="footer-mail-signup-submit">submit</button>
    </div>
    <div class="footer-social-media-ctas">
        <?php
        $hkfootersmmenu = array (
            'theme_location' 	=> 'social-media-menu',
            'container' 		=> 'nav',
            'container_id'		=> 'footer-social-media',
            'menu_class'		=> 'navbar list-unstyled',
            'menu_id' => 'footer-social-media-menu',
            'container_class' 	=> 'footer-social-media-menu'
        );
        wp_nav_menu($hkfootersmmenu);
        ?>
    </div>
    <div>
        <?php
        $hkfootermenu = array(
            'theme_location' => 'footer-menu',
            'container' => 'nav',
            'container_id' => 'footermenu',
            'menu_class' => 'navbar list-unstyled',
            'menu_id' => 'footer-menu'
            //'container_class' 	=> 'col-sm-6'
        );
        wp_nav_menu($hkfootermenu);
        ?>
    </div>
    <div>
      © <?php echo date('Y'); ?> Honey's Kettle Restaurant & Bakery
    </div>
  </div>

</div>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/slick.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/js/bootstrap.min.js"
        integrity="sha384-XEerZL0cuoUbHE4nZReLT7nx9gQrQreJekYhJD9WNWhH8nEW+0c5qq7aIo2Wl30J"
        crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/honeyskettle.js"></script>

<!-- sandbox testing -->
<!--<script src="https://www.paypal.com/sdk/js?client-id=AfgavxcaDmNifD-ZTgION9vm3idEoaYjdjw89STXXvjjEwczcp5Rw9WbgKMi0TazbL9BtEotrBffrlZM&disable-funding=credit,card"> // Required. Replace SB_CLIENT_ID with your sandbox client ID.</script>-->

<!-- live prod -->
<!--<script src="https://www.paypal.com/sdk/js?client-id=AXoY82I0nUkuEChxBBGCWaDWwcy0bjTtN7WZt_JST5PvmdpTinqhdA-QnCeTBWvOkJU522sD5-LfQfcE&currency=USD&disable-funding=credit,card"> // Required. Replace SB_CLIENT_ID with your sandbox client ID.</script>-->
<?php
/* include google analytics code */
include 'includes/googleAnalytics.php';
?>

</body>
</html>
