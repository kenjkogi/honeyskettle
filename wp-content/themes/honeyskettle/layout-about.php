<?php
/**
 * Template Name: Our Story Template
 * The template for displaying Our Story page.
 *
 * This is the template that displays the Our Story page.
 *
 * @package honey's Kettle
 * @since honeyskettle 1.0
 */

?>

<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <div class="our-story-hero">
        <h1><?php the_title(); ?></h1>
    </div>
    <div>
        <?php echo get_the_content(); ?>
    </div>

<?php endwhile;  ?>

<?php get_footer(); ?>
