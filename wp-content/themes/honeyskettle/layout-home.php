<?php
/**
 * Template Name: Home Template
 * The template for displaying the home page.
 *
 * This is the template that displays the Our Story page.
 *
 * @package honey's Kettle
 * @since honeyskettle 1.0
 */

?>

<?php get_header(); ?>

<!-- carousel -->
<div class="uppercase container-fluid">
    <div id="home-hero" class="row carousel slide" data-ride="carousel" data-interval="10000">
      <div class="slides list-unstyled carousel-inner">

          <?php
          $args = array('category' => '13', 'order' => 'ASC');
          $heroposts = get_posts($args);
          foreach ($heroposts as $key => $post) :
              setup_postdata($post);
              $heroimgurl = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
              $curvedimage = get_post_meta($post->ID, 'home-slider-curved', true);
              $sliderContent = get_the_content();
              $currentIndex = array_search($post ,array_keys($heroposts));
              ?>

            <div class="slide carousel-item <?php echo $key === 0 ? 'active' : ''?>" style="background-image: url(<?php echo $heroimgurl; ?>);">
              <div class="home-hero-content">
              </div>
            </div>
          <?php
          endforeach;
          wp_reset_postdata();
          ?>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
</div>

<!-- channels call out -->
<div class="featured-networks container-fluid">
  <div class="home-app-promotion">
    <p>Download the App</p>
    <a href="https://apps.apple.com/us/app/honeys-kettle-to-go/id1164536986" target="_blank">
      <img class="download-honeys-kettle-app" src="/wp-content/themes/honeyskettle/images/download-apple-app.png"
           alt="Download the app">
    </a>
    <a href="https://play.google.com/store/apps/details?id=com.chownow.honeyskettletogo" target="_blank">
      <img class="download-honeys-kettle-app" src="/wp-content/themes/honeyskettle/images/download-google-app.png"
           alt="Download the app">
    </a>
  </div>
  <div class="vendors">
    order online from:
    <span class="chownow-link">
      <a class="chownow-order-online" href="https://ordering.chownow.com/order/6191/locations" target="_blank">
        <img src="/wp-content/themes/honeyskettle/images/home-vendor-chownow.png" alt="ChowNow">
      </a>
    </span>
    <span class="uber-eats-link">
      <a class="uber-eats-order-online"
         href="https://www.ubereats.com/los-angeles/food-delivery/honeys-kettle-drop-kitchen/BT03JpoaT-iJKml5adh4gg"
         target='_blank'>
        <img src="/wp-content/themes/honeyskettle/images/home-vendor-uber-eats.png" alt="Uber Eats">
      </a>
    </span>
    <span class="grubhub-link">
      <a class="grubhub-order-online"
         href="https://www.grubhub.com/restaurant/honeys-kettle-fried-chicken-358-w-38th-st-los-angeles/2258415"
         target='_blank'>
        <img src="/wp-content/themes/honeyskettle/images/home-vendor-grubhub.png" alt="GrubHub">
      </a>
    </span>
    <span class="doordash-link">
      <a class="doordash-order-online" href="https://www.doordash.com/store/honey-s-kettle-los-angeles-1055707/en-US"
         target='_blank'>
        <img src="/wp-content/themes/honeyskettle/images/home-vendor-doordash.png" alt="Doordash">
      </a>
    </span>
    <span class="postmates-link">
      <a class="postmates-order-online" href="https://postmates.com/merchant/honey-drop-kitchen-los-angeles-39587"
         target='_blank'>
        <img src="/wp-content/themes/honeyskettle/images/home-vendor-postmates.png" alt="Postmates">
      </a>
    </span>
  </div>
  <div class="three-home-links">
    <ul class="list-unstyled row three-home-links-row">
      <li class="col three-home-link three-home-link-locations link-1">
        <a href="<?php echo site_url() ?>/locations">locations</a>
      </li>
      <li class="col three-home-link three-home-link-menu link-2">
        <a href="<?php echo site_url() ?>/menu">menu</a>
      </li>
      <li class="col three-home-link three-home-link-shop link-3">
        <a href="http://honeyskettle.shop">shop</a>
      </li>
    </ul>
  </div>
</div>

<!--3 blocks -->
<div class="home-summary container-fluid">
  <div class="row home-three-home-blocks">
    <!--    <div class="col">-->
      <?php
      $args = array(
          'meta_key' => 'is-featured-on-home-page',
          'meta_value' => 'true',
          'orderby' => 'is-featured-on-home-page-order',
          'order' => 'asc'
      );
      $homePageFeaturedPages = get_pages($args);
      foreach ($homePageFeaturedPages as $page) :
          $pageTitle = get_post_meta($page->ID, 'is-featured-on-home-page-title', true);
          $pageSummary = get_the_excerpt($page);
          $pageLink = '<a href="' . get_permalink($page) . '">learn more</a>';
          $featuredImage = '<img src="' . get_post_meta($page->ID, 'is-featured-on-home-page-image', true) . '"/>';
          $featuredImageLink = get_post_meta($page->ID, 'is-featured-on-home-page-image', true);

          ?>
        <div class="home-three-blocks-block col col-12">
          <div class="row home-three-blocks-block-row">
            <div class="col col-12 col-md-6 home-three-blocks-block-image"
                 style="background-image: url('<?php echo $featuredImageLink; ?>')">
              <div class="row"><?php echo $featuredImage; ?></div>
            </div>
            <div class="col col-12 col-md-6 home-three-blocks-block-desc">
              <div class="home-three-blocks-block-desc-title"><?php echo $pageTitle; ?></div>
              <div class="home-three-blocks-block-desc-summary"><?php echo $pageSummary; ?></div>
              <div class="home-three-blocks-block-desc-page-link"><?php echo $pageLink ?></div>
            </div>
          </div>
        </div>
      <?php
      endforeach;
      wp_reset_postdata();
      ?>
    <!--    </div>-->
  </div>
</div>


<?php get_footer(); ?>
