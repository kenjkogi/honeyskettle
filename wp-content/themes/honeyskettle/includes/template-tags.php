<?php
/**
 * The template for displaying all other pages.
 *
 * This is the template that displays all other pages by default.
 *
 * @package My Security Training
 * @since mysecuritytraining 1.0
 */

?>



<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <div class="col-sm-12">
        <h1 class="text-center uppercase"><?php the_title(); ?></h1>
    </div>
    <div class="col-sm-12">
        <?php echo get_the_content(); ?>
    </div>

<?php endwhile;  ?>


<?php get_footer(); ?>

