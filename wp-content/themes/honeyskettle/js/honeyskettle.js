$(function() {

  // This adds 'placeholder' to the items listed in the jQuery .support object.
  jQuery(function() {
    jQuery.support.placeholder = false;
    test = document.createElement('input');
    if ('placeholder' in test) jQuery.support.placeholder = true;
  });
  // This adds placeholder support to browsers that wouldn't otherwise support it.
  $(function() {
    if (!$.support.placeholder) {
      var active = document.activeElement;
      $(':text').focus(function() {
        if ($(this).attr('placeholder') !== '' && $(this).val() === $(this).attr('placeholder')) {
          $(this).val('').removeClass('hasPlaceholder');
        }
      }).blur(function() {
        if ($(this).attr('placeholder') !== '' && ($(this).val() === '' || $(this).val() === $(this).attr('placeholder'))) {
          $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
        }
      });
      $(':text').blur();
      $(active).focus();
      $('form:eq(0)').submit(function() {
        $(':text.hasPlaceholder').val('');
      });
    }
  });



  function is_touch_device() {
    return !!('ontouchstart' in window);
  }

  $('.carousel').carousel();

  $('#mobile-menu-trigger').click( function() {

  //  find out if menu is open
      // check it mobile-menu-items has the class open

    var $menuItems = $('.mobile-menu-items');
    var $menuIsOpen = $menuItems.hasClass('open');
    if( $menuIsOpen) {
      $menuItems.removeClass('open');
      console.log('removing class');
    } else {
      $menuItems.addClass('open');
      console.log('adding class');
    }
  //  if not open open it
      // add a class open to mobile-menu-items
  //  if open close it
      // remove the class open to mobile-menu-items

  });

  // submit email stuff
  $('.footer-mail-signup-submit').click( function() {
    console.log('submit clicked');
  //  ajax to add to DB of email subscriptions
  });

  // locations pixelate function
  $('.drop-location-item').mouseenter( function() {
    console.log('hovered', this);
    $('.drop-location-item').not( this ).addClass('blur');
    // $('.drop-location-item').not('')
  } ).mouseleave( function() {
    console.log('cursor left', this);
    $('.drop-location-item').not( this ).removeClass('blur');
  } );

  setTimeout(function() {
    OnScreenSizeChange()
  }, 0);


  $(window).resize(function() {
    OnScreenSizeChange()
  });

  function OnScreenSizeChange() {
  //  do screen resie stuff here

  }


});
