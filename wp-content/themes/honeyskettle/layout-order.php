<?php
/**
 * Template Name: Locations Template
 * The template for displaying the locations page.
 *
 * This is the template that displays locations page.
 *
 * @package honey's Kettle
 * @since honeyskettle 1.0
 */

?>


<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
    <div>
        <?php echo get_the_content(); ?>
    </div>

<?php endwhile;  ?>

<?php get_footer(); ?>
