<?php
/**
 * Template Name: Posts/Press
 * The template for displaying the press page.
 *
 * This is the template that displays the Press page.
 *
 * @package honey's Kettle
 * @since honeyskettle 1.0
 */

?>

<?php get_header(); ?>

<div class="press-hero">
    <h1>Press</h1>
</div>

<div class="press-releases">
    
<?php
  $args = array(
    'category' => '16',
    'order' => 'DESC',
    'numberposts' => 100
  );
$pressPosts = get_posts($args);
foreach ($pressPosts as $post) :
    setup_postdata($post);
    $pressItemImage = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
    $pressItemContent = get_the_content();
    $pressItemLink = get_post_meta($post->ID, 'press-post-link', true);;
    $pressItemTitle = $post->post_title;
    $pressItemDate = get_the_time('m.d.Y', $post->ID);
    ?>

    <div class="press-item">
        <div class="container">
            <div class="row">
                <div class="col col-md-2 col-12">
                    <img src="<?php echo $pressItemImage; ?>" alt="<?php echo $pressItemTitle; ?>">
                </div>
                <div class="col">
                    <p class="press-title"><b><?php echo $pressItemTitle ?></b> <small><?php echo $pressItemDate; ?></small></p>
                    <p class="press-description">
                        <?php echo $pressItemContent; ?>
                    </p>
                    <p class="press-link">
                        <a href="<?php echo $pressItemLink; ?>" target="_blank">Full Story</a>
                    </p>
                </div>
            </div>
        </div>
    </div>


<?php
endforeach;
wp_reset_postdata();
?>

</div>

<?php get_footer(); ?>
