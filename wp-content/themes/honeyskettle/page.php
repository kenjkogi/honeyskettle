<?php
/**
 * The template for displaying all other pages.
 *
 * This is the template that displays all other pages by default.
 *
 * @package Honey's Kettle
 * @since honeyskettle 1.0
 */

?>

<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <div>
        <h1 class="text-center uppercase"><?php the_title(); ?></h1>
    </div>
    <div>
        <?php echo get_the_content(); ?>
    </div>

<?php endwhile;  ?>

<?php get_footer(); ?>
