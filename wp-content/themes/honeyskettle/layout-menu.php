<?php
/**
 * Template Name: Menu Template
 * The template for displaying the menu page.
 *
 * This is the template that displays the Menu page.
 *
 * @package honey's Kettle
 * @since honeyskettle 1.0
 */

?>


<?php get_header(); ?>

<div class="dish-menu-hero">
  <h1>Menu</h1>
</div>
<div class="dish-menu">
  <ul class="nav nav-tabs nav-justified" id="dish-menu-tabs" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="dish-menu-dine-in-tab" data-toggle="tab" href="#dish-menu-dine-in" role="tab"
         aria-controls="home" aria-selected="true">dine in</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="dish-menu-catering-tab" data-toggle="tab" href="#dish-menu-catering" role="tab"
         aria-controls="profile" aria-selected="false">catering</a>
    </li>
  </ul>
  <div class="tab-content" id="dish-menu-tab-content">


    <div class="tab-pane fade show active" id="dish-menu-dine-in" role="tabpanel" aria-labelledby="dish-menu-dine-in">
      <div class="container">


        <div class="accordion" id="dish-menu-dine-in-accordion">

          <?php
//          get all categories under menu

          $categoryArgs = array(
            'child_of' => 6,
            'orderby' => 'id',
            'order' => 'ASC'

          );
          $categories = get_categories( $categoryArgs );
          $categoriesCounter = 0;
          $categoriesLength = count($categories);
          foreach ( $categories as $category ) :
              $categoryFrontEndId = $category->slug . '-' . $category->cat_ID;
              $categoryShown = $categoriesCounter == 0 ? 'show' : '';
          ?>
          <div class="row accordion-button-row">
            <div class="col accordion-button-col">
              <button
                class="btn btn-link btn-block text-left"
                type="button"
                data-toggle="collapse"
                data-target="#<?php echo $categoryFrontEndId; ?>"
              >
                <!--                change above to category id -->
                <?php echo $category->cat_name ?>
              </button>
            </div>
          </div>

          <div
            id="<?php echo $categoryFrontEndId; ?>"
            class="collapse <?php echo $categoryShown; ?>"
            data-parent="#dish-menu-dine-in-accordion"
          >
            <ul class="dish-menu-items row list-unstyled">
                <?php
                  $categoryItemsArgs = array(
                      'category' => $category->cat_ID,
                      'order' => 'ASC',
                      'numberposts'	=> 100
                  );
                  $mealItems = get_posts($categoryItemsArgs);
                  foreach ($mealItems as $mealItem) :
                    setup_postdata($mealItem);
                    $mealTitle = $mealItem->post_title;
                    $mealDescription = get_the_excerpt($mealItem);
                    $mealImage = wp_get_attachment_url(get_post_thumbnail_id($mealItem->ID));
                ?>
                    <li class="col col-12 col-sm-6 dish-menu-dine-in-item">
                      <div class="row">
                        <div class="col col-12 col-md-3 dish-menu-dine-in-item-image">
                          <img src="<?php echo $mealImage; ?>" alt="<?php echo $mealTitle; ?>">
                        </div>
                        <div class="col col-12 col-md-9 dish-menu-dine-in-item-description">
                          <h3><?php echo $mealTitle; ?></h3>
                          <p><?php echo $mealDescription; ?></p>
                        </div>
                      </div>
                    </li>

                <?php
                  endforeach;
                  wp_reset_postdata();
                ?>

            </ul>
          </div>

          <?php
            $categoriesCounter++;
            endforeach;
          ?>

        </div>

      </div>
    </div>
    <div class="tab-pane fade" id="dish-menu-catering" role="tabpanel" aria-labelledby="dish-menu-catering">
      <div class="container">
        <div class="row">
          <div class="col">
            <p>Whether you’re having a picnic, party or with family, Honey’s Kettle would love to cater your next gathering and look forward to serving you our fresh, handcrafted meals. For more information about our catering menu, please give us a call or send us an email. All orders require 24-48 Hours notice and a member of our Crew will get back to you.</p>
            <p>Phone: (310) 202-5453</p>
            <p>Email: catering@honeyskettle.com</p>
          </div>
        </div>


        <div class="accordion" id="dish-menu-catering-accordion">

            <?php
            //          get all categories under menu

            $categoryArgs = array( 'child_of' => 12 );
            $categories = get_categories( $categoryArgs );
            $categoriesCounter = 0;
            $categoriesLength = count($categories);
            foreach ( $categories as $category ) :
                $categoryFrontEndId = $category->slug . '-' . $category->cat_ID;
                $categoryShown = $categoriesCounter == 0 ? 'show' : '';
                ?>
              <div class="row">
                <div class="col">
                  <button
                    class="btn btn-link"
                    type="button"
                    data-toggle="collapse"
                    data-target="#<?php echo $categoryFrontEndId; ?>"
                    aria-expanded="true"
                    aria-controls="collapseOne"
                  >
                    <!--                change above to category id -->
                      <?php echo $category->cat_name ?>
                  </button>
                </div>
              </div>

              <div
                id="<?php echo $categoryFrontEndId; ?>"
                class="collapse <?php echo $categoryShown; ?>"
                data-parent="#dish-menu-catering-accordion"
              >
                <ul class="dish-menu-items row list-unstyled">
                    <?php
                    $categoryItemsArgs = array(
                        'category' => $category->cat_ID,
                        'order' => 'ASC'
                    );
                    $mealItems = get_posts($categoryItemsArgs);
                    foreach ($mealItems as $mealItem) :
                        setup_postdata($mealItem);
                        $mealTitle = $mealItem->post_title;
                        $mealDescription = get_the_excerpt($mealItem);
                        $mealImage = wp_get_attachment_url(get_post_thumbnail_id($mealItem->ID));
                        ?>
                      <li class="col dish-menu-catering-item">
                        <div class="row">
                          <div class="col dish-menu-catering-item-image">
                            <img src="<?php echo $mealImage; ?>" alt="<?php echo $mealTitle; ?>">
                          </div>
                          <div class="col dish-menu-catering-item-description">
                            <h3><?php echo $mealTitle; ?></h3>
                            <p><?php echo $mealDescription; ?></p>
                          </div>
                        </div>
                      </li>

                    <?php
                    endforeach;
                    wp_reset_postdata();
                    ?>

                </ul>
              </div>

                <?php
                $categoriesCounter++;
            endforeach;
            ?>

        </div>

      </div>
    </div>
  </div>

</div>

<?php get_footer(); ?>
